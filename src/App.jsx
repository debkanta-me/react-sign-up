import React, { Component } from "react";
import Form from "./components/Form";
import Header from "./components/Header";
import ErrorMessage from "./components/ErrorMessage";
import Welcome from "./components/Welcome";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errors: [],
      errorBool: false,
      successfullyLogged: false,
    };
  }

  validateForm = (errorMessages) => {
    if (errorMessages.length > 0) {
      this.setState({
        errors: errorMessages,
        errorBool: true,
      });
    } else {
      this.setState({
        errors: errorMessages,
        errorBool: false,
        successfullyLogged: true,
      });
    }
  };

  render() {
    return (
      <div>
        {this.state.successfullyLogged && <Welcome />}
        {!this.state.successfullyLogged && (
          <div className="container">
            <Header />
            <div className="errors">
              {this.state.errorBool &&
                this.state.errors.map((message, index) => (
                  <ErrorMessage key={index} message={message} />
                ))}
            </div>
            <Form validateForm={this.validateForm} />
          </div>
        )}
      </div>
    );
  }
}

export default App;
