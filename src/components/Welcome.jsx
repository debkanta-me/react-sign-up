import React from "react";

export default function Welcome() {
  return (
    <div>
      <h1 style={{ margin: "0 auto" }}>Hello, world!</h1>
    </div>
  );
}
