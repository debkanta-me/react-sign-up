import React, { Component } from "react";
import Input from "./Input";
import "./Form.css";

export class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      confirmPassword: "",
      errorMessages: [],
    };
  }

  handleInputChange = (e) => {
    if (e.placeholder === "First Name") {
      this.setState({ firstName: e.value });
    } else if (e.placeholder === "Last Name") {
      this.setState({ lastName: e.value });
    } else if (e.placeholder === "Email Id") {
      this.setState({ email: e.value });
    } else if (e.placeholder === "Password") {
      this.setState({ password: e.value });
    } else {
      this.setState({ confirmPassword: e.value });
    }
  };

  handleSubmission(e) {
    const regex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (
      this.state.firstName === "" ||
      this.state.lastName === "" ||
      this.state.email === "" ||
      this.state.password === "" ||
      this.state.confirmPassword === ""
    ) {
      this.setState({
        errorMessages: this.state.errorMessages.push(
          "Fill all the required fields.."
        ),
      });
    }

    if (!regex.test(String(this.state.email).toLowerCase())) {
      this.setState({
        errorMessages: this.state.errorMessages.push(
          "Email address not valid.."
        ),
      });
    }

    if (
      this.state.password.length < 8 ||
      this.state.confirmPassword.length < 8
    ) {
      this.setState({
        errorMessages: this.state.errorMessages.push(
          "Password length should be minimum 8.."
        ),
      });
    }

    if (this.state.password !== this.state.confirmPassword) {
      this.setState({
        errorMessages: this.state.errorMessages.push(
          "Passwords doesn't match.."
        ),
      });
    }

    e.preventDefault();

    this.setState({
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      confirmPassword: "",
      errorMessages: [],
    });

    this.props.validateForm(this.state.errorMessages);
  }

  render() {
    return (
      <div>
        <form onSubmit={(e) => this.handleSubmission(e)}>
          <Input
            type={"text"}
            value={this.state.firstName}
            placeholder={"First Name"}
            handleChange={this.handleInputChange}
          />
          <Input
            type={"text"}
            value={this.state.lastName}
            placeholder={"Last Name"}
            handleChange={this.handleInputChange}
          />
          <Input
            type={"text"}
            value={this.state.email}
            placeholder={"Email Id"}
            handleChange={this.handleInputChange}
          />
          <Input
            type={"password"}
            value={this.state.password}
            placeholder={"Password"}
            handleChange={this.handleInputChange}
          />
          <Input
            type={"password"}
            value={this.state.confirmPassword}
            placeholder={"Confirm Password"}
            handleChange={this.handleInputChange}
          />
          <div className="button">
            <button className="btn btn-success" type="submit">
              Sign Up
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Form;
