import React from "react";
import "./Input.css";

function Input(props) {
  return (
    <div className="input">
      <input
        type={props.type}
        value={props.value}
        placeholder={props.placeholder}
        onChange={(e) => {
          props.handleChange(e.target);
        }}
        className="form-control"
      />
    </div>
  );
}

export default Input;
