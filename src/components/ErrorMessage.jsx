import React from "react";
import "./ErrorMessage.css";

export default function ErrorMessage(props) {
  return (
    <div className="alert alert-danger error">
      <p>{props.message}</p>
    </div>
  );
}
